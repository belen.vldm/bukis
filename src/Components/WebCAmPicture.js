import React, { useRef, useState } from 'react';
import Webcam from 'react-webcam';

function WebCAmPicture() {
    const webcam = useRef(Webcam);
    const [image,setImage] = useState();
    const [landmarkWebCamPicture,setLandmarkWebCamPicture] = useState();

    const videoConstraints = {
        width: 1280,
        height: 720,
        facingMode: "user"
    };

    const capture = React.useCallback(() => {
        const imageSrc = webcam.current.getScreenshot();
        setImage(imageSrc)
        setLandmarkWebCamPicture(image)
    }, [webcam]);

    return (<>
        {landmarkWebCamPicture}
        <Webcam audio={false} ref={webcam} style={{position: "absolute", margin: "auto", textAlign: "center", top: 100, left: 0, right: 0,}} screenshotFormat="image/jpeg" videoConstraints={videoConstraints} />
        {image!=''?
            <button onClick={(e)=>{e.preventDefault();
                setImage('')
            }} className="webcam-btn">Retake Image</button>
        :
            <button onClick={(e)=>{e.preventDefault();
                capture();
            }} className="webcam-btn">Capture</button>
        }
    </>)
}
export default WebCAmPicture;