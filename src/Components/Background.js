import React, {  useRef } from 'react';
import { Canvas } from "@react-three/fiber";
import { OrbitControls, Stars } from '@react-three/drei';
export default function Backgroud() {
    const groupRef = useRef();
    const date = new Date();
	const night = ((date.getHours() >= 19 && date.getHours() <= 23) || (date.getHours() >= 0 && date.getHours() <= 4)) ? true : false;
	const day = (night) ? 'MidnightBlue' : 'DeepSkyBlue';
    return (
        <Canvas id="background" style={{background: day}} >
            <group ref={groupRef}>
                <Stars radius={100} depth={50} count={3000} factor={6} saturation={0} fade />
            </group>
		</Canvas>
    );
}