import * as canvas from 'canvas';
import * as faceapi from "face-api.js";

export async function loadModels() {
    console.log("MODELS: ", faceapi.nets);
    const MODEL_URL = process.env.PUBLIC_URL + '/models';
    await faceapi.loadTinyFaceDetectorModel(MODEL_URL);
    await faceapi.loadFaceLandmarkTinyModel(MODEL_URL);
    await faceapi.loadFaceRecognitionModel(MODEL_URL);
    await faceapi.loadMtcnnModel(MODEL_URL);
    await faceapi.loadFaceLandmarkModel(MODEL_URL);
    await faceapi.loadFaceExpressionModel(MODEL_URL);
}

const { Canvas, Image, ImageData } = canvas
export async function getFullFaceDescription(blob, inputSize = 512) {
    loadModels()
    // faceapi.env.monkeyPatch({ Canvas, Image, ImageData })
    faceapi.env.monkeyPatch({
        Canvas: HTMLCanvasElement,
        Image: HTMLImageElement,
        ImageData: ImageData,
        Video: HTMLVideoElement,
        createCanvasElement: () => document.createElement('canvas'),
        createImageElement: () => document.createElement('img')
    });
    
    // tiny_face_detector options
    let scoreThreshold = 0.5;
    const OPTION = new faceapi.TinyFaceDetectorOptions({
        inputSize,
        scoreThreshold
    });
    const useTinyModel = true;

    // const detections = await faceapi.detectAllFaces(blob, OPTION)
    // console.log("REFERENCEIMAGE:", blob);
    let referenceImage = await faceapi.fetchImage(blob);
    const canvas = faceapi.createCanvasFromMedia(referenceImage);
    // const canvas = faceapi.createCanvas(1280,720);
    // const referenceImage = await loadImage(blob);
    console.log("REFERENCEIMAGE:", referenceImage);
    const displaySize = { width: referenceImage.width, height: referenceImage.height };
    let detections = await faceapi.detectAllFaces(referenceImage, OPTION).withFaceLandmarks(useTinyModel).withFaceExpressions();//.withAgeAndGender();
    // let drawBox = [];
    const resizedDetections = faceapi.resizeResults(detections, displaySize);
    // canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
    faceapi.draw.drawDetections(canvas, resizedDetections);
    // faceapi.draw.drawFaceLandmarks(canvas, resizedDetections);
    // faceapi.draw.drawFaceExpressions(canvas, resizedDetections);
    // faceapi.draw.drawFaceExpressions(canvas, resizedDetections);
    // resizedDetections.forEach( detection => {
    //     const box = detection.detection.box
    //     drawBox = new faceapi.draw.DrawBox(box)
    //     drawBox.draw(canvas)
    // })
    // return canvas;
    return canvas;
}