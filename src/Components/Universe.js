import React, {useEffect, useRef, useState} from 'react';
import { OrbitControls, Stars } from '@react-three/drei';
import Buki from './Buki';
import Terrain from './Terrain';
// import { useFrame } from '@react-three/fiber';
import axios from 'axios';

export default function Universe(props) {
    const planeColor = (props.night) ? '#1bbc82' : '#45deab'; //SlateGrey, AliceBlue, 006400
    const wireframeColor = (props.night) ? '#45deab' : '#1bbc82';
    const ambientColor = (props.night) ? 'LightSteelBlue' : 'Yellow';
    const pointColor = (props.night) ? 'snow' : 'Beige';
    const ambientInt = props.ambientInt;
    const pointInt = props.pointInt;
    const bukiInit = props.buki;
    const activeAll = props.activeAll;
    const [buki, setBuki] = useState(bukiInit);
    let url = `https://mundo-bukis.herokuapp.com/api/world/${bukiInit.hashCode}/${bukiInit.mood}/${bukiInit.update['touch']}/${bukiInit.args}/`
    useEffect(() => {
        const interval = setInterval(() => {
            if (buki.id !== '') {
                url = `https://mundo-bukis.herokuapp.com/api/world/${buki.hashCode}/${buki.mood}/${buki.update['touch']}/${buki.args}/${buki.emotion}/`;
                // url = `http://localhost:5000/api/world/${buki.hashCode}/${buki.mood}/${buki.update['touch']}/${buki.args}/${buki.emotion}/`;
            }
            
            axios.get(url).then(res => {
                if (res.status === 200) {
                    setBuki(res.data[0]);
                } else if (buki.id === '' && res.status === 204) {
                    setBuki(bukiInit);
                }
            })
            .catch(err => console.log("CATCH getWorld:", err) );
        }, 2000);
        return () => clearInterval(interval);
    }, [buki])
    
    const [allBukis, setAllBukis] = useState(null);
    useEffect(() => {
		if (activeAll) {
			axios.get(`https://mundo-bukis.herokuapp.com/api/world/bukis/`).then(res => {
                console.log("RES: ", res);
                if (res.status == 200) {
                    if (allBukis == null) {
                        setAllBukis(res.data);
                    } else if (Object.keys(res.data).length != Object.keys(allBukis).length) {
                        console.log("RES DATA: ", res.data);
                        setAllBukis(res.data);
                    }
                }
			})
			.catch(err => {
				console.log("CATCH getAllBukis:", err);
				setAllBukis("Ocurrio un error, vuelva a intentarlo...");
			});
		} else {
			setAllBukis(null);
		}
	})

    const groupRef = useRef();
    // useFrame(({ clock }) => {
    //     console.log("**************************");
    //     console.log("clock:", clock.getElapsedTime());
    //     console.log("**************************");

    //     if(clock.getElapsedTime() > 10 && clock.getElapsedTime() < 20) {
    //         const a = clock.getElapsedTime(); // giro continuo
    //         groupRef.current.rotation.x = a;
    //     } else if(clock.getElapsedTime() > 20 && clock.getElapsedTime() < 30) {
    //         const a = Math.sin(clock.getElapsedTime()); // vaiven
    //         groupRef.current.rotation.x = a;
    //     } else if(clock.getElapsedTime() > 30 && clock.getElapsedTime() < 40) {
    //         groupRef.current.rotation.y += 0.005;
    //     } else if(clock.getElapsedTime() > 40) {}
    // });
    return (
        <group ref={groupRef} dispose={null}>
            <ambientLight color={ambientColor} intensity={ambientInt} />
            <pointLight position={[0, 6, 8]} intensity={pointInt} color={pointColor} />
            {props.night?
                <Stars radius={100} depth={50} count={3000} factor={6} saturation={0} fade />
            : '' }
                {/* Factor: size (10) Saturation: colores (10) */}
            <Buki activeAll={false} position={[0, 0.2, 0]} moods={buki.mood} args={buki.args} emotion={buki.emotion} />
            {allBukis?
                allBukis.map((bukiw, key) => (
                    <Buki activeAll={activeAll} key={key} position={[(Math.random() * (-40 + key) + (key * 3)), 0.8, -(Math.random() * (80 - 40) + 40)]} moods={bukiw.mood} args={bukiw.args} />
                ))
            : ''}
            <Terrain color={planeColor} wireframe={wireframeColor} night={props.night} />
            {/* <OrbitControls />  */}
            <OrbitControls minDistance={2} maxDistance={10} maxPolarAngle={1.5} minPolarAngle={1} maxAzimuthAngle={1} minAzimuthAngle={-1} dampingFactor={0.01} />
        </group>
    );
}