import { useRef } from "react";
import { useFrame } from "@react-three/fiber";
import { Plane, MeshDistortMaterial } from "@react-three/drei";

const GROUND_HEIGHT = -1;
const GROUND_Z = -900;

export default function Terrain(props) {
    const { color, wireframe, night } = props
    const width = 1000;
    const height = 2000;
    const terrain = useRef();

    useFrame(() => {
        terrain.current.position.z += 0.15;
        if (terrain.current.position.z > 600) {
            terrain.current.position.z = GROUND_Z;
        }
    });
    
    return (
        <mesh visible position={[0, GROUND_HEIGHT, GROUND_Z]} rotation={[-Math.PI / 2, 0, 0]} ref={terrain} >
            <planeBufferGeometry attach="geometry" args={[width, height, 128, 128]} />
            <meshStandardMaterial attach="material" color={wireframe} roughness={100} metalness={0} wireframe />
            <Plane args={[width, height]}>
                {night ?
                    <MeshDistortMaterial attach="material" color={color} />
                    :
                    <meshBasicMaterial attach="material" color={color} />
                }
            </Plane>
        </mesh>
    )
}