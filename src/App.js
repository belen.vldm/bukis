import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Buki from "./Pages/Buki";
import World from "./Pages/World";

function App() {
    return (
        <div className="App">
            <BrowserRouter>
                <Switch>
                    <Route path="/World" component={World} />
                    <Route path="/" component={Buki} />
                </Switch>
            </BrowserRouter>
        </div>
    );
}
export default App;