import React, { Suspense, useEffect, useLayoutEffect, useMemo, useRef, useState, } from 'react' //useState, useCallback, useEffect
import * as THREE from "three";
import { useFrame, useLoader } from "@react-three/fiber";
import { animated } from '@react-spring/three' //useTransition, useSpring,
// import neutral from '../emotions/neutral.svg';
import Neutral from '../emotions/neutral.svg';
import Feliz from '../emotions/happy.svg';
import Triste from '../emotions/sad.svg';
import Enojadx from '../emotions/angry.svg';
import Asustadx from '../emotions/fearful.svg';
import Enfermx from '../emotions/disgusted.svg';
import Sorprendidx from '../emotions/surprised.svg';

import { SVGLoader } from 'three/examples/jsm/loaders/SVGLoader'

export default function Mood(moodProps) {
    let { position, mood, args, emotion, activeAll } = moodProps;
    const mesh = useRef();
    const eyes = useRef();
    // const meshFear = useRef();
    // const [isHovered, setIsHovered] = useState(false);
    const loader = new SVGLoader();
    const svgResource = new Promise(resolve =>
        {if (emotion == "Neutral") {
            loader.load(Neutral, (shapes) => {
                resolve(shapes.paths.map((group, key) => {
                    return group.toShapes(true).map(shape => {
                        const fillColor = group.userData.style.fill
                        return ({ shape, color: fillColor, key })
                    })
                }))
            })
        } else if (emotion == "Feliz") {
            loader.load(Feliz, (shapes) => {
                resolve(shapes.paths.map((group, key) => {
                    return group.toShapes(true).map(shape => {
                        const fillColor = group.userData.style.fill
                        return ({ shape, color: fillColor, key })
                    })
                }))
            })
        } else if (emotion == "Triste") {
            loader.load(Triste, (shapes) => {
                resolve(shapes.paths.map((group, key) => {
                    return group.toShapes(true).map(shape => {
                        const fillColor = group.userData.style.fill
                        return ({ shape, color: fillColor, key })
                    })
                }))
            })
        } else if (emotion == "Enojadx") {
            loader.load(Enojadx, (shapes) => {
                resolve(shapes.paths.map((group, key) => {
                    return group.toShapes(true).map(shape => {
                        const fillColor = group.userData.style.fill
                        return ({ shape, color: fillColor, key })
                    })
                }))
            })
        } else if (emotion == "Asustadx") {
            loader.load(Asustadx, (shapes) => {
                resolve(shapes.paths.map((group, key) => {
                    return group.toShapes(true).map(shape => {
                        const fillColor = group.userData.style.fill
                        return ({ shape, color: fillColor, key })
                    })
                }))
            })
        } else if (emotion == "Enfermx") {
            loader.load(Enfermx, (shapes) => {
                resolve(shapes.paths.map((group, key) => {
                    return group.toShapes(true).map(shape => {
                        const fillColor = group.userData.style.fill
                        return ({ shape, color: fillColor, key })
                    })
                }))
            })
        } else if (emotion == "Sorprendidx") {
            loader.load(Sorprendidx, (shapes) => {
                resolve(shapes.paths.map((group, key) => {
                    return group.toShapes(true).map(shape => {
                        const fillColor = group.userData.style.fill
                        return ({ shape, color: fillColor, key })
                    })
                }))
            })
        }}
    )
    function SvgShape({shape, color, colorKey, index}) {
        const mesh = useRef();
        let colorMesh = colorKey ? colorKey : color;
        return (
            <mesh ref={mesh}>
                <shapeBufferGeometry attach="geometry" args={[shape]} />
                <meshBasicMaterial aspect={window.innerWidth / window.innerHeight} attach="material" color={colorMesh} opacity={1} side={THREE.DoubleSide} flatShading={true} depthWrite={true} polygonOffset polygonOffsetFactor={index * -0.1} />
            </mesh>
        );
    }
    function Scene(props) {
        const { positionEyes, scaleEye, colorKey } = props
        console.log("DATA: ", positionEyes, scaleEye, colorKey);
        const [shapes, set] = useState([]);
        useEffect(() => svgResource.then(set), []);
        return (
            <group
                position={positionEyes}
                scale={scaleEye}
                rotation={[THREE.Math.degToRad(0), THREE.Math.degToRad(180), THREE.Math.degToRad(180)]}>
                {shapes.map(item => 
                    <SvgShape key={item[0].shape.uuid} colorKey={colorKey} {...item[0]} />
                )}
            </group>
        );
    }
    
    // const color = isHovered ? 0xe5d54d : 0xf95b3c;
    const colorDef = activeAll? new THREE.Color().setHSL((Math.random() * (0.3 - 0.1) + 0.1), 1, 0.5) : new THREE.Color( 0x5F9EA0 );
    const colorOne = activeAll? new THREE.Color().setHSL((Math.random() * (0.3 - 0.1) + 0.1), 1, 0.5) : new THREE.Color().setHex( Math.random() * 0x00ff00 );
    const colorTwo = activeAll? new THREE.Color().setHSL((Math.random() * (0.3 - 0.1) + 0.1), 1, 0.5) : new THREE.Color().setHex( Math.random() * 0xffff00 );
    const colorThree = activeAll? new THREE.Color().setHSL((Math.random() * (0.3 - 0.1) + 0.1), 1, 0.5) : new THREE.Color().setHex( Math.random() * 0xff0000 );
    
    // let wink = 2 * Math.PI; //|| 2
    // useFrame(({clock}) => {
    //     clock.getDelta();
    //     // args: isActive ? [0.2, 6, 0, 2 * Math.PI] : [0.2, 6, 0, 2],
    //     console.log("USEFRAME", clock.getDelta());
    // });
    // const [isActive, setIsActive] = useState(false);
    // const springs = useSpring({
        // args: isActive ? [0.2, 6, 0, 2 * Math.PI] : [0.2, 6, 0, 2],
    // })
    console.log("activeAll: ", activeAll);
    console.log("position: ", position);
    console.log("MOOD: ", mood);
    console.log("ARGS: ", args);
    console.log("EMOTION: ", emotion);
    
    if (mood === 1) { // joy, yellow
        return (
            <mesh ref={mesh} position={position} visible castShadow>
                <Scene positionEyes={[-0.3, 0.3, 0.44]} scaleEye={[0.1, 0.1, 0.1]} />
                <sphereGeometry args={activeAll? [Math.random() * (3 - 0.5) + 0.5, Math.round(Math.random() * (12 - 6) + 6), Math.round(Math.random() * (20 - 2) + 2)] : args} attach="geometry" />
                {/* {[0.5,12,8]} radius:Float, widthSegments:Integer, heightSegments:Integer, phiStart:Float, phiLength:Float, thetaStart:Float, thetaLength:Float */}
                <meshStandardMaterial attach="material" color={colorOne} roughness={0.5} metalness={0.2} flatShading={true} />
            </mesh>
        )
        } else if (mood === 2) { // sadness, steelblue
        return (
            <mesh ref={mesh} position={position} visible castShadow>
                <Scene positionEyes={[-0.5, 0, 1.28]} scaleEye={[0.2, 0.2, 0.2]} />
                <coneGeometry args={activeAll? [Math.random() * (5 - 0.5) + 0.5, Math.random() * (10 - 1) + 1, Math.round(Math.random() * (24 - 4) + 4)] : args} attach="geometry" />
                {/* <coneGeometry args={[1, 2, 10]} attach="geometry" /> */}
                {/* radius:Float, height:Float, radialSegments:Integer */}
                <meshStandardMaterial attach="material" color={colorTwo} roughness={0.5} metalness={0.5} flatShading={true} />
            </mesh>
        )
    } else if (mood === 3) { // fear, mediumpurple
        return (
            <mesh ref={mesh} position={position} visible castShadow>
                <Scene positionEyes={[-0.5, 1, 2.20]} scaleEye={[0.2, 0.2, 0.2]} />
                <tetrahedronGeometry args={activeAll? [Math.random() * (3 - 0.5) + 0.5, Math.round(Math.random() * (20 - 1) + 1)] : args} attach="geometry" />
                {/* [1, 2] radius:Float, detail:Integer */}
                <meshStandardMaterial attach="material" color={colorThree} roughness={0.5} metalness={0.2} flatShading={true} />
            </mesh>
        )
    } else if (mood === 4) { // disgustin, seagreen
        // {/* <mesh onClick={e => onClick(e)} onPointerOver={e => onHover(e, true)} onPointerOut={e => onHover(e, false)}> */}        
        return (
            <mesh ref={mesh} position={position} visible castShadow>
                <Scene positionEyes={[-0.5, 2, 2.03]} scaleEye={[0.2, 0.2, 0.2]} />
                <cylinderGeometry args={activeAll? [Math.random() * (4 - 0.4) + 0.4, Math.random() * (4 - 0.4) + 0.4, Math.round(Math.random() * (6 - 1) + 1), Math.round(Math.random() * (24 - 4) + 4)] : args} attach="geometry" />
                {/* [0.4, 0.6, 2, 10], radiusTop:Float, radiusBottom:Float, height:Float, radialSegments:Integer*/}
                <meshStandardMaterial attach="material" color={colorOne} roughness={0.5} metalness={0.2} flatShading={true} />
                {/* <meshDistanceMaterial color='red' /> */}
            </mesh>
        )
    } else if (mood === 5) { // anger, DarkRed
        return (
            <mesh ref={mesh} position={position} visible castShadow>
                <Scene positionEyes={[-0.6, 0.5, 3.005]} scaleEye={[0.2, 0.2, 0.2]} />
                <octahedronGeometry args={activeAll? [Math.random() * (4 - 0.5) + 0.5, Math.round(Math.random() * (16 - 2) + 2)] : args} attach="geometry" />
                {/* radius : Float, detail : Integer */}
                <meshStandardMaterial attach="material" color={colorTwo} roughness={0.5} metalness={0.2} flatShading={true} />
                {/* <meshNormalMaterial attach="material" color="DarkRed" /> */}
            </mesh>
        )
    } else if (mood === 6) { // joyful, greenyellow
        return (
            <mesh ref={mesh} position={position} visible castShadow>
                <Scene positionEyes={[0.04, -0.5, 0.4]} scaleEye={[0.07, 0.07, 0.07]} colorKey="LemonChiffon" />
                <torusGeometry args={activeAll? [Math.random() * (3 - 0.4) + 0.4, Math.random() * (2 - 0.4) + 0.4, Math.round(Math.random() * (16 - 1) + 1), Math.round(Math.random() * (12 - 1) + 1)] : args} attach="geometry" />
                {/* radius:Float, tube:Float, radialSegments:Integer, tubularSegments:Integer, arc:Float */}
                <meshStandardMaterial attach="material" color={colorThree} roughness={0.2} metalness={0.2} flatShading={true} />
            </mesh>
        )
    } else { // indifferent, CadetBlue
        return (
            <mesh ref={mesh} position={position} visible castShadow>
                <Scene positionEyes={[-0.6, 1, 1.04]} scaleEye={[0.2, 0.2, 0.2]} />
                <boxGeometry args={args} attach="geometry" />
                <meshStandardMaterial attach="material" color={colorDef} roughness={0.2} metalness={0.2} flatShading={true} />
            </mesh>
        )
    }
}