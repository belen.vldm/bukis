import React, { useEffect, useRef, useState } from "react";
import * as faceApi from "face-api.js";
import { Button, Col, Container, Row, Modal, Alert, Offcanvas, Card, ButtonToolbar, Spinner } from 'react-bootstrap';
import axios from "axios";

function postEmotion(expressions, expressionMapEs, buki, creator) {
    const data = expressions.sort((a, b) => b[1] - a[1]).filter((_, i) => i < 1).map(([e]) => {
        let exp = Object.keys(expressionMapEs).find(keyMap => expressionMapEs[keyMap] === e);
        return exp;
    });
    if (data){
        // axios.get(`http://localhost:5000/api/bukis/emotions/${data}/${buki.hashCode}/${creator}`).then(res => {
        axios.get(`https://mundo-bukis.herokuapp.com/api/bukis/emotions/${data}/${buki.hashCode}/${creator}`).then(res => {
            console.log("postEmotion:", res);
        })
        .catch(err => {
            console.log("CATCH postEmotion:", err);
        });
    }
}

function FaceApi(props) {
    const {buki, creator, closeModal} = props;
    const video = useRef();
    const [expressions, setExpressions] = useState([]);
    const [unmounted, setUnmounted] = useState(false);

    const expressionMap = {
        neutral: "😶",
        happy: "😄",
        sad: "😞",
        angry: "🤬",
        fearful: "😖",
        disgusted: "🤢",
        surprised: "😲"
    };
    const expressionMapEs = {
        Neutral: "😶",
        Feliz: "😄",
        Triste: "😞",
        Enojadx: "🤬",
        Asustadx: "😖",
        Enfermx: "🤢",
        Sorprendidx: "😲"
    };
    
    useEffect(() => {
        run()
        return () => { console.log("unmounted..."); setUnmounted(true) }
    }, [])
    const run = async () => {
        console.log("run started");
        try {
            await faceApi.nets.tinyFaceDetector.load("/models/");
            await faceApi.loadFaceExpressionModel(`/models/`);
            let mediaStream = await navigator.mediaDevices.getUserMedia({
                video: { facingMode: "user" }
            });
            video.current.srcObject = mediaStream;
        } catch (e) {
            console.log("ERROR: ", e.name, e.message, e.stack);
        }
    };
  
    const onPlay = async () => {
        if (!unmounted && video.current != null) {
            if (video.current.paused || video.current.ended || !faceApi.nets.tinyFaceDetector.params) {
                setTimeout(() => onPlay());
                return;
            }
            const options = new faceApi.TinyFaceDetectorOptions({inputSize: 512, scoreThreshold: 0.5});
            const result = await faceApi.detectSingleFace(video.current, options).withFaceExpressions();
        
            if (result) {
                const expression = Object.keys(result.expressions).reduce((acc, prev, key) => {
                    acc.push([expressionMap[prev], result.expressions[prev].toFixed(2)]);
                    return acc;
                }, []);
                setExpressions(expression)
            }
            setTimeout(() => onPlay(), 100);
        }
    };    
    return (
        <Container fluid className="FaceApi">
            {/* <Spinner animation="border" role="status" /> */}
            <Row className="justify-content-center">
				<Col xs={12}>
                    <p>Emociones encontradas..</p>
                </Col>
                {expressions.sort((a, b) => b[1] - a[1]).filter((_, i) => i < 3).map(([e, w], key) => (
                    <Col xs={12} sm={4} key={key}>
                        <p>{e} {w}<br/>{Object.keys(expressionMapEs).find(keyMap => expressionMapEs[keyMap] === e)}</p>
                    </Col>
                ))}
            </Row>
            <Row>
                <Col xs={12}>
                    <div style={{ width: "100%", height: "50vh", position: "relative" }}>
                        <video ref={video} autoPlay muted onPlay={onPlay} style={{
                            position: "absolute", width: "100%", height: "50vh", left: 0, right: 0, bottom: 0, top: 0
                        }} />
                    </div>
                </Col>
                <Col xs={{ span: 3, offset: 9 }}>
                    <Button className="mt-4 secondary" onClick={() => {
                        postEmotion(expressions, expressionMapEs, buki, creator);
                        closeModal();
                    }}>Reflejarme</Button>
                </Col>
            </Row>
        </Container>
    );
}
export default FaceApi;