import React, { Suspense, useState } from 'react'
import './World.css';
import { Canvas } from "@react-three/fiber";
import Universe from '../Components/Universe';
// import Sound from 'react-sound';
import { Button, Col, Container, Row, Modal, Alert, Offcanvas, Card, ButtonToolbar, Spinner } from 'react-bootstrap';
import FaceApi from "../Components/FaceApi"

function World(props) {
	const buki = props.location.state.data;
	const creator = props.location.state.creator;
	const date = new Date();
	const night = ((date.getHours() >= 19 && date.getHours() <= 23) || (date.getHours() >= 0 && date.getHours() <= 4)) ? true : false;
	const day = (night) ? 'MidnightBlue' : 'DeepSkyBlue';
	let ambientInt = 0.06;
	let pointInt = 0.4;
	if (date.getHours() >= 5 && date.getHours() < 7) {
		ambientInt = 0.04;
		pointInt = 0.4;
	} else if (date.getHours() >= 7 && date.getHours() < 10) {
		ambientInt = 0.06;
		pointInt = 0.5;
	} else if (date.getHours() >= 10 && date.getHours() < 19) {
		ambientInt = 0.08;
		pointInt = 0.5;
	} else if (date.getHours() >= 19 && date.getHours() <= 23) {
		ambientInt = 0.03;
		pointInt = 0.4;
	} else if (date.getHours() >= 0 && date.getHours() < 5) {
		ambientInt = 0.02;
		pointInt = 0.20;
	}
	// const [isPlaying, setIsPlaying] = useState(true);
	const [show, setShow] = useState(false);
	const [activeAll, setActiveAll] = useState(false);
	const closeModal = () => {setShow(false);};
	return (
		<Container fluid className="World" style={{background: day}}>
			<Row>
				{/* <Sound
					url="../../public/sounds/moon.mp3"
					playStatus={isPlaying ? Sound.status.PLAYING : Sound.status.STOPPED}
					playFromPosition={300}
					// onLoading={this.handleSongLoading}
					// onPlaying={this.handleSongPlaying}
					// onFinishedPlaying={this.handleSongFinishedPlaying}
				/> */}
				<Col>
					<ButtonToolbar className="justify-content-between">
						<Button variant="primary" size="sm" onClick={()=> setActiveAll(!activeAll)}>{activeAll? 'Mi Buki' : 'Otras Buki'}</Button>
						<a href="https://forms.gle/8Agx3NE582CMM8n57" target="_blank">Tu feedback es muy importante 😊</a>
						{/* <Button variant="primary" size="sm" onClick={() => setIsPlaying(!isPlaying)}> {isPlaying ? 'Stop' : 'Play'}</Button> */}
						<Button variant="secondary" onClick={() => setShow(true)} size="sm">Reflejo</Button>
						<Modal show={show} onHide={() => setShow(false)} aria-labelledby="modalFaceApi" centered>
							<Modal.Header>
								<Modal.Title id="modalFaceApi"><p>Reflejate en <span>{buki.name}</span></p></Modal.Title>
							</Modal.Header>
							<Modal.Body>
								<FaceApi buki={buki} creator={creator} closeModal={closeModal} />
							</Modal.Body>
						</Modal>
					</ButtonToolbar>
				</Col>
			</Row>
			<Row>
				<Col className="m-0 p-0">
					<Canvas style={{background: day, height:'93.2vh'}} >
						{console.log("activeAll", activeAll)}
						<Universe night={night} buki={buki} activeAll={activeAll} ambientInt={ambientInt} pointInt={pointInt} />
					</Canvas>
				</Col>
			</Row>
		</Container>
	);
}
export default World;