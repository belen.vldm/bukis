import React, { useState, useRef, useEffect } from 'react'
import Webcam from "react-webcam";
import { getFullFaceDescription, loadModels } from '../Components/FaceApi';

// import * as faceapi from "face-api.js";

// const Face = () => {
//     const video = useRef(Webcam);
//     const [image,setImage] = useState();
//     const videoConstraints = {
//         width: 1280,
//         height: 720,
//         facingMode: "user"
//     };
    
//     // function startVideo() {
//     //     navigator.getUserMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
//     //     navigator.getUserMedia({video: {}}, stream => video.srcObject = stream, err => console.log(err))
//     // }
//     // startVideo();

//     useEffect(async ()=> {
//         console.log("MODELS: ", faceapi.nets);
//         const MODEL_URL = process.env.PUBLIC_URL + '/models';
//         await faceapi.loadTinyFaceDetectorModel(MODEL_URL);
//         await faceapi.loadFaceLandmarkTinyModel(MODEL_URL);
//         await faceapi.loadFaceRecognitionModel(MODEL_URL);
//         await faceapi.loadMtcnnModel(MODEL_URL);
//         await faceapi.loadFaceLandmarkModel(MODEL_URL);
//         await faceapi.loadFaceExpressionModel(MODEL_URL);
//     });

//     const capture = React.useCallback(() => {
//         const imageSrc = video.current.getScreenshot();
//         setImage(imageSrc)
//         runFaceDetect()
//     }, [video]);

//     const runFaceDetect = (async () => {
//         const canvas = faceapi.createCanvasFromMedia(image);
//         document.body.append(canvas);
//         const displaySize = { width: video.width, height: video.height };
//         faceapi.matchDimensions(canvas, displaySize);
//         setInterval(async ()=> {
//             const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceExpressions().withAgeAndGender();
//             const resizedDetections = faceapi.resizeResults(detections, displaySize);
//             canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
//             faceapi.draw.drawDetections(canvas, resizedDetections);
//             faceapi.draw.drawFaceLandmarks(canvas, resizedDetections);
//             faceapi.draw.drawFaceExpressions(canvas, resizedDetections);
//             faceapi.draw.drawFaceExpressions(canvas, resizedDetections);
//             resizedDetections.forEach( detection => {
//                 const box = detection.detection.box
//                 const drawBox = new faceapi.draw.DrawBox(box, { label: Math.round(detection.age) + " year old " + detection.gender })
//                 drawBox.draw(canvas)
//             })
//         }, 100);
//     });

//     return (<>
//         <Webcam audio={false} ref={video} style={{position: "absolute", margin: "auto", textAlign: "center", top: 100, left: 0, right: 0,}} screenshotFormat="image/jpeg" videoConstraints={videoConstraints} />
//         <img src={image} style={{position: 'absolute', top: 600}} />

//          {image!=''?
//              <button onClick={(e)=>{e.preventDefault();
//                  setImage('')
//              }} className="webcam-btn">Retake Image</button>
//          :
//              <button onClick={(e)=>{e.preventDefault();
//                  capture();
//              }} className="webcam-btn">Capture</button>
//          }
//      </>)
// }
// export default Face;

const Face = () => {
    const webcam = useRef(Webcam);
    const [image,setImage] = useState();

    const videoConstraints = {
        width: 1280,
        height: 720,
        facingMode: "user"
    };

    const capture = React.useCallback(() => {
        const imageSrc = webcam.current.getScreenshot();
        setImage(imageSrc)
        runFaceDetect()
    }, [webcam]);

    const runFaceDetect = async () => {
        // await loadModels();

        // await getFullFaceDescription(image).then(fullDesc => {
        //     console.log("FULLDESC:", fullDesc);
        //     return fullDesc;
        // }).catch(err => {
        //     console.log("pred ERR:", err);
        // });

        if (webcam.current) {
            const webcamCurrent = webcam.current;
            if (webcamCurrent.video.readyState === 4) {
                const predictions = await getFullFaceDescription(image, 160).then(fullDesc => {
                    console.log("FULLDESC:", fullDesc);
                    return fullDesc;
                }).catch(err => {
                    console.log("pred ERR:", err);
                });
                if (predictions.length) {
                    console.log("PREDICTIONS:", predictions);
                }
            }
        };
    }
    return (<>
        <Webcam audio={false} ref={webcam} style={{position: "absolute", margin: "auto", textAlign: "center", top: 100, left: 0, right: 0,}} screenshotFormat="image/jpeg" videoConstraints={videoConstraints} />

        <img src={image} style={{position: 'absolute', top: 600}} />

        {image!=''?
            <button onClick={(e)=>{e.preventDefault();
                setImage('')
            }} className="webcam-btn">Retake Image</button>
        :
            <button onClick={(e)=>{e.preventDefault();
                capture();
            }} className="webcam-btn">Capture</button>
        }
    </>)
}
export default Face;