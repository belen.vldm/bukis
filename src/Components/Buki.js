import React from 'react'
import Mood from './Mood'

export default function Buki(props) {
    const { position, moods, args, emotion, activeAll }  = props;

    return (
        <Mood mood={moods} position={position} args={args} emotion={emotion} activeAll={activeAll} />
    );
}