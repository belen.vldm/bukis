import 'bootstrap/dist/css/bootstrap.min.css';
import './Bukis.css';
import React, { useState } from 'react' //useEffect,
import { Redirect, Route } from 'react-router-dom';
import axios from "axios";
import { Button, Col, Container, Form, Row, Alert, Modal, Card, Badge, Carousel, Image } from 'react-bootstrap';
import Background from '../Components/Background';
import firefox from "../instrucciones/instrucciones.mp4";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHandPointer } from '@fortawesome/free-solid-svg-icons'
import { faCopyright, faArrowAltCircleDown } from '@fortawesome/free-regular-svg-icons'
import { faLinkedinIn } from '@fortawesome/free-brands-svg-icons'

const Buki = () => {
    const date = new Date();
	const night = ((date.getHours() >= 19 && date.getHours() <= 23) || (date.getHours() >= 0 && date.getHours() <= 4)) ? true : false;
    const [buki, setBuki] = useState('');
    const [creator, setCreator] = useState('');
    const [hashCode, setHashCode] = useState('');
    const [isLoading, setLoading] = useState(false);
    const [show, setShow] = useState(false);
    
    function getBuki(hashCode, creator) {
        setLoading(true)
        // axios.get(`http://localhost:5000/api/bukis/${hashCode}/${creator}`).then(res => {
        axios.get(`https://mundo-bukis.herokuapp.com/api/bukis/${hashCode}/${creator}`).then(res => {
            setBuki(res.data);
            setLoading(false);
        })
        .catch(err => {
            console.log("CATCH getBuki:", err);
            setBuki("Ocurrio un error, vuelva a intentarlo...");
            setLoading(false);
        });
    }
    
    function HandleChange(event) {
        setCreator(event.target.value);
    }
    
    function HandleGet(event) {
        setHashCode(event.target.value);
    }

    function About() {
        // {night? {color: '#94edc9'} : {color: '#222'}
        return (<>
            <p style={night? {color: '#c5ceef'} : {color: '#091e68'}}><FontAwesomeIcon icon={faCopyright} /> 2021 | Tesis Lic. en Tecnología Multimedial | UMAI | <a id="bldm" className={night? 'bldmNight' : 'bldmDay' } href="https://www.linkedin.com/in/belulopezdelmonte" target="_blank">María Belén López del Monte <FontAwesomeIcon icon={faLinkedinIn} />.</a></p>
        </>);
    }

    return (
        <Container className="bukis">
            <Row className="mb-5 getBukis"> 
                <Col md={12} className="mb-5">
                    <h1 className="title">Bienvenido a Bukis!</h1>
                    <h2 className="subtitle">Digitalizando emociones</h2>
                </Col>
                <Col md={{span: 6, offset: 3}} className="mb-4">
                    <p className="subtitle" style={night? {color: '#c5ceef'} : {color: '#091e68'}}>Este site es parte de una propuesta artística biométrica interactiva y colaborativa, que busca acortar la distancia social, impuesta por la pandemia por covid-19, entre las personas y sus seres queridos.</p>
                </Col>
            </Row>
            <Row className="text-center mb-4 justify-content-center">
                <Col md={4}>
                    <Card className="cards">
                        <Card.Body>
                            <Card.Title><h3><Badge bg="info" pill>1</Badge>  Interactuar</h3></Card.Title>
                            <Card.Text className="p-2">
                                Mirá las <a id="instrucciones" onClick={() => setShow(true)}>instrucciones</a> para descargar la <a id="download" href="https://mundo-bukis.herokuapp.com/download" download>APP <FontAwesomeIcon icon={faArrowAltCircleDown} color="#c5ceef" size="sm" /></a> en tu smartphone o tablet Android. Y creá e interactuá con las Bukis
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col md={4}>
                    <Card className="cards">
                        <Card.Body>
                            <Card.Title><h3><Badge bg="info" pill>2</Badge>  Compartir</h3></Card.Title>
                            <Card.Text className="p-2">
                                Para ver la variación de la Buki, mediada por la participación biométrica ingresá al mundo Buki <span className="handPointer"><FontAwesomeIcon icon={faHandPointer} flip="vertical" color="#c5ceef" size="lg" /></span>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col md={4}>
                    <Card className="cards">
                        <Card.Body>
                            <Card.Title><h3><Badge bg="info" pill>3</Badge>  Acercar</h3></Card.Title>
                            <Card.Text className="p-2">
                                Usá la propuesta artística biométrica interactiva y colaborativa para estar más cerca de quienes queres!
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            
            <Modal size="lg" show={show} onHide={() => setShow(false)} aria-labelledby="modalInstrucciones" centered>
                <Modal.Header closeButton>
                    <Modal.Title id="modalInstrucciones"><p>¿Cómo interactuar?</p></Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <video width="720" height="405" controls loop autoPlay>
                        <source src={firefox} type="video/mp4"></source>
                    </video>
                    {/* <Image src={firefox} fluid rounded /> */}
                    {/* <Carousel>
                        <Carousel.Item>
                            <img className="d-block w-25 rounded" src={firefox1} alt="First slide" />
                            <Carousel.Caption>
                                <h3>First slide label</h3>
                                <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                            </Carousel.Caption>
                        </Carousel.Item>
                    </Carousel> */}
                </Modal.Body>
            </Modal>

            <Row xs sm className="form g-3">
                <Col xs sm md={{ span: 4, offset: 4 }} lg={{ span: 4, offset: 4 }}>
                    <Card>
                        <Card.Title>Mundo Buki!</Card.Title>
                        <Card.Body>
                            <Col className="hidden">
                                <Form.Control type="text" placeholder="Tu alias/nombre" value={creator} onChange={HandleChange} />
                                <Form.Control type="text" placeholder="el hash" value={hashCode} onChange={HandleGet} />
                                <div className="d-grid gap-2">
                                    <Button className="btn" type="button" variant="primary" disabled={isLoading} onClick={ () => getBuki(hashCode, creator) }>
                                        {isLoading ? 'Cargando…' : 'Ingresar!'}
                                    </Button>
                                </div>
                            </Col>
                            <Col className="hidden disclaimer">
                                <Alert key="disc" variant="warning">Para mejor visualización y funcionamiento ingresá desde la compu</Alert>
                            </Col>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <Row>
                <Col>
                    {typeof buki === 'object' ?
                        // <h2>Bienvenido {buki.name}!</h2>
                        <Route><Redirect to={{ pathname:"./World", state: {data: buki, creator: creator}}} /></Route>
                        :
                        (buki !== '' ? <Col md={{ span: 4, offset: 4 }} lg={{ span: 4, offset: 4 }}><Alert className="mt-3" variant="danger">{buki}</Alert></Col> : "")
                    }
                </Col>
            </Row>
            <Row className="footer pt-5">
                <Col>
                    <About />
                </Col>
            </Row>
            <Background />
        </Container>
    )
}
export default Buki;